#ifndef MATRIXUTIL_H
#define MATRIXUTIL_H

#define BOOST_UBLAS_TYPE_CHECK 0

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/range/algorithm_ext/erase.hpp>
#include <boost/qvm/all.hpp>
#include <boost/qvm/mat_operations.hpp>
#include <map>
#include <vector>
#include <string>
#include "matrixutil.hpp"
#include "object.h"

class MatrixUtil
{
    public:

        MatrixUtil();
        static std::map<std::string, std::vector<Object>> separate(std::vector<Object> objects);
        static std::map<std::string, boost::numeric::ublas::matrix<float>> prepareClassesMatrices(std::map<std::string, std::vector<Object>> classes);
        static boost::numeric::ublas::matrix<float> subtractMeansFromClass(boost::numeric::ublas::matrix<float> singleClass, std::vector<float> means);
        static boost::numeric::ublas::matrix<float> prepareClassMatrix(std::vector<Object> singleClass);
        static std::vector<float> calculateMeans(boost::numeric::ublas::matrix<float> singleClass);
        static boost::numeric::ublas::matrix<float> calculateCovarianceMatrix(boost::numeric::ublas::matrix<float> singleClass);
        static float matrixDistance(boost::numeric::ublas::matrix<float> m);
        static float det_fast(const boost::numeric::ublas::matrix<float>& matrix);

};

#endif // MATRIXUTIL_H
