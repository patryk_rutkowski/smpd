#ifndef CLASSIFIER_H
#define CLASSIFIER_H

#include <string>
#include <vector>
#include <math.h>

#include "object.h"

class ClassifierCalc
{
private:
    std::vector<Object> trainObjects;
    std::vector<Object> testObjects;
    size_t noOfTrainObjects;
    size_t noOfTestObjects;

    bool compareTestObjectWithTrainVector(Object testObject);
    double computeComparation(std::vector<float> trainFeatures, std::vector<float> testFeatures, int numberOfFeatures);

public:

    ClassifierCalc(){}

    std::vector<Object> getTrainObjects(){return trainObjects;}
    std::vector<Object> getTestObjects(){return testObjects;}

    int execute(std::vector<Object> testObjects, std::vector<Object> trainObjects);

};


#endif // CLASSIFIER_H
