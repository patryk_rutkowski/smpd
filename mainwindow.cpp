#include "mainwindow.h"
#include "ui_mainwindow.h"



#include <QImage>
#include <QDebug>
#include <vector>

#define BOOST_UBLAS_TYPE_CHECK 0

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/qvm/all.hpp>
#include <boost/qvm/mat_operations.hpp>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    FSupdateButtonState();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateDatabaseInfo()
{
    ui->FScomboBox->clear();
    for(unsigned int i=1; i<=database.getNoFeatures(); ++i)
        ui->FScomboBox->addItem(QString::number(i));

    ui->FStextBrowserDatabaseInfo->setText("noClass: " +  QString::number(database.getNoClass()));
    ui->FStextBrowserDatabaseInfo->append("noObjects: "  +  QString::number(database.getNoObjects()));
    ui->FStextBrowserDatabaseInfo->append("noFeatures: "  +  QString::number(database.getNoFeatures()));

}

void MainWindow::FSupdateButtonState(void)
{
    if(database.getNoObjects()==0)
    {
        FSsetButtonState(false);
    }
    else
        FSsetButtonState(true);

}


void MainWindow::FSsetButtonState(bool state)
{
   ui->FScomboBox->setEnabled(state);
   ui->FSpushButtonCompute->setEnabled(state);
   ui->FSradioButtonFisher->setEnabled(state);
   ui->FSradioButtonSFS->setEnabled(state);
}

void MainWindow::on_FSpushButtonOpenFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open TextFile"), "", tr("Texts Files (*.txt)"));

    if ( !database.load(fileName.toStdString()) )
        QMessageBox::warning(this, "Warning", "File corrupted !!!");
    else
        QMessageBox::information(this, fileName, "File loaded !!!");

    FSupdateButtonState();
    updateDatabaseInfo();
}

void MainWindow::on_FSpushButtonCompute_clicked()
{
    int dimension = ui->FScomboBox->currentText().toInt();

    if(ui->FSradioButtonSFS ->isChecked())
    {
            float FLD = 0, tmp;
            int max_ind = -1;

            for (uint i = 0; i < database.getNoFeatures(); ++i)
            {
                std::map<std::string, float> classAverages;
                std::map<std::string, float> classStds;

                for (auto const &ob : database.getObjects())
                {
                    classAverages[ob.getClassName()] += ob.getFeatures()[i];
                    classStds[ob.getClassName()] += ob.getFeatures()[i] * ob.getFeatures()[i];
                }

                std::for_each(database.getClassCounters().begin(), database.getClassCounters().end(), [&](const std::pair<std::string, int> &it)
                {
                    classAverages[it.first] /= it.second;
                    qDebug()<< classAverages[it.first];
                    classStds[it.first] = std::sqrt(classStds[it.first] / it.second - classAverages[it.first] * classAverages[it.first]);
                }
                );

                tmp = std::abs(classAverages[ database.getClassNames()[0] ] - classAverages[database.getClassNames()[1]]) / (classStds[database.getClassNames()[0]] + classStds[database.getClassNames()[1]]);

                if (tmp > FLD)
                {
                    FLD = tmp;
                    max_ind = i;
                }

              }

              std::vector<int> tempVec = sfs.execute(database.getObjects(), dimension, max_ind);
              for(int i=0;i<tempVec.size();i++){
                  ui->FStextBrowserDatabaseInfo->append("ind: "  +  QString::number(tempVec.at(i)));
              }
     }
    else if (ui->FSradioButtonFisher->isChecked()) {
        std::vector<int> tempVec = fisher.execute(database.getObjects(), dimension);
        for(int i=0;i<tempVec.size();i++){
            ui->FStextBrowserDatabaseInfo->append("ind: "  +  QString::number(tempVec.at(i)));
        }
    }
}

void MainWindow::on_CpushButtonOpenFile_clicked()
{
    on_FSpushButtonOpenFile_clicked();
}

// rozdzielenie wartosci na dwa wektory osobny dla quercusa osobny dla acera
void MainWindow::divideValues(std::vector<Object> Objects, int numberOfParts) {

    std::vector<Object> quercusValues;
    std::vector<Object> acerValues;

    for (Object trainingObject : Objects) {

        if(trainingObject.getClassName().compare("Quercus") == 0)
            quercusValues.push_back(trainingObject);
        else
            acerValues.push_back(trainingObject);
    }

    partsOfObjects.clear();

    // wyliczamy ile obiektów znajdzie się w jednym przedziale
    size_t quercusPart = quercusValues.size()/numberOfParts;
    size_t acerPart = acerValues.size()/numberOfParts;

    // wrzucamy odpowiednie elementy do przedziału tak by były tam elementy z obu klas i mieszamy je
    for(int i=0;i<numberOfParts;i++){
        std::vector<Object> tmp = std::vector<Object>(quercusValues.begin()+(quercusPart*i), quercusValues.begin()+(quercusPart*(i+1)));
        tmp.insert(tmp.end(), acerValues.begin()+(acerPart*i), acerValues.begin()+(acerPart*(i+1)));
        std::random_shuffle (tmp.begin(), tmp.end());
        partsOfObjects.push_back(tmp);
    }
}

//metoda wykonywana w momencie wciśnięcia przycisku Train
void MainWindow::on_CpushButtonTrain_clicked()
{
     int percentageTrainValues = ui->CplainTextEditTrainingPart->toPlainText().toInt();
     if (percentageTrainValues >= 100 || percentageTrainValues <= 0)
     {
         QMessageBox::warning(this, "Warning", "Value should be in range 1-99");
     }
     else
     {
         // pobieranie z bazy danych ilości wszystkich obiektów
         double numberOfAllObjects = database.getNoObjects();

         int numberOfTrainObjects = numberOfAllObjects * (percentageTrainValues/100.0);
         ui->CtextBrowser->append("Train objects: "  +  QString::number(numberOfTrainObjects) + "/" + QString::number(numberOfAllObjects));

         // przypisanie do wektora obiektów z bazy danych i pomieszanie ich
         std::vector<Object> objects = database.getObjects();
         std::random_shuffle ( objects.begin(), objects.end());

         // przypisanie początkowych wartości do treningowego wektora i reszty do testowego
         trainPartOfObjects = std::vector<Object>(objects.begin(), objects.begin() + numberOfTrainObjects);
         testPartOfObjects = std::vector<Object>(objects.begin() + numberOfTrainObjects, objects.end());

         trainPartOfObjects = std::vector<Object>(objects.begin(), objects.begin() + numberOfTrainObjects);
         testPartOfObjects = std::vector<Object>(objects.begin() + numberOfTrainObjects, objects.end());
     }
}

// metoda wykonywana w momencie przyciśnięcia Execute
void MainWindow::on_CpushButtonExecute_clicked()
{
    // wykonanie metody getPercentage i wyświetlenie jej wyniku
    int percentage = getPercentage(trainPartOfObjects, testPartOfObjects);
    ui->CtextBrowser->append("Good classification: "  +  QString::number(percentage) + "%");
}


/**
  Bootstrap
 * @brief MainWindow::on_pushButton_clicked
 */
void MainWindow::on_pushButton_clicked()
{
    int percentageTrainValues = ui->CplainTextEditTrainingPart->toPlainText().toInt();
    int numberOfAllObjects = database.getNoObjects();
    int numberOfTrainObjects = numberOfAllObjects * (percentageTrainValues/100.0);
    std::vector<int> results;
    std::vector<Object> objects = database.getObjects();
    srand(time(NULL));

    for (int i = 0; i < ui->CcomboBoxK->currentIndex() + 1; i++) {
        //Przepisanie wartośći z oryginalnego vectora do tymczasowego
        std::vector<Object> allObjects = std::vector<Object>(objects.begin(), objects.end());
        std::vector<Object> trainObject;

        //Losowe wybieranie próbek (ilość określona przez użytkownika)
        for (int j = 0; j < numberOfTrainObjects; j++) {
            int randIndex = rand()%allObjects.size();
            trainObject.push_back(allObjects.at(randIndex));
            allObjects.erase(allObjects.begin() + randIndex);
        }

        
        results.push_back(getPercentage(trainObject, allObjects));
    }

    int sum = 0;
    //Średnia z wyników klasyfikacji
    for (int i = 0; i < results.size(); i++)
        sum += results.at(i);

    ui->CtextBrowser->append("Bootstrap: "  +  QString::number(sum/results.size()) + "%");
}

// przekazanie do metody wartości treningowych i testowych, i w zależności od metody wybranej w liście uzyskiwane są wyniki.
int MainWindow::getPercentage(std::vector<Object> train, std::vector<Object> test) {

    switch(ui->CcomboBoxClassifiers->currentIndex()) {
        case 0:
            return classifierCalc.execute(train, test);
        case 1:
            return knnClassifier.execute(train, test, ui->CcomboBoxK->currentIndex()+1);
        case 2:
            return nmclassifier.execute(train, test);
        case 3:
            return knmClassifier.execute(train, test, ui->CcomboBoxK->currentIndex()+1);
    }
}

// wybieramy grupe treningowa która  stanowia obiekty wszystkie prócz tego który stanowi grupe testowa
std::vector<Object> MainWindow::getTrainingObjectsForPart(int partId){
    std::vector<Object> tmp;
    for(int i=0;i<partsOfObjects.size();i++){
        if(i==partId)
            continue;
        tmp.insert(tmp.end(),partsOfObjects.at(i).begin(),partsOfObjects.at(i).end());
    }
    return tmp;
}

// wcisniecie przycisku crosswalidacji
void MainWindow::on_crossvalidationPushButton_clicked()
{
    int percentage = 0;
    // pobieramy na ile uzytkownik chce podzielic podprzedziałów
    int amountOfTrainValues = ui->CplainTextEditTrainingPart->toPlainText().toInt();
    std::vector<Object> objects = database.getObjects();
    // mieszamy dane i je dzielimy na podprzedziałe
    std::random_shuffle ( objects.begin(), objects.end());
    divideValues(objects, amountOfTrainValues);
    ui->CtextBrowser->append("Train objects divided");

    // przechodzimy po wszystkich przedziałach pobieramy ich wyniki % i wyliczamy z nich srednią
    for(int i=0; i<partsOfObjects.size();i++){
        percentage += getPercentage(getTrainingObjectsForPart(i), partsOfObjects.at(i));
    }
    percentage/=partsOfObjects.size();

    ui->CtextBrowser->append("Crossvalidation: "  +  QString::number(percentage) + "%");
}
