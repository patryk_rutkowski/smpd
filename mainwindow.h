#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <fstream>
#include <QCheckBox>
#include <QFileDialog>
#include <QtCore>
#include <QtGui>
#include <QMessageBox>


#include "database.h"
#include "classifierCalc.h"
#include "knnclasifier.h"
#include "nmclassifier.h"
#include "knmclassifier.h"
#include "fisher.h"
#include "sfs.h"
#include "matrixutil.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    bool loadFile(const std::string &fileName);
    void updateDatabaseInfo();
    void FSupdateButtonState(void);
    void FSsetButtonState(bool state);
    int getPercentage(std::vector<Object> train, std::vector<Object> test);
    void divideValues(std::vector<Object> Objects, int numberOfParts);
    std::vector<Object> getTrainingObjectsForPart(int partId);



private slots:
    void on_FSpushButtonOpenFile_clicked();
    void on_FSpushButtonCompute_clicked();
    void on_CpushButtonOpenFile_clicked();
    void on_CpushButtonTrain_clicked();
    void on_CpushButtonExecute_clicked();
    void on_pushButton_clicked();
    void on_crossvalidationPushButton_clicked();

private:
    Ui::MainWindow *ui;

private:
     Database database;
     ClassifierCalc classifierCalc;
     KNNClassifier knnClassifier;
     NMClassifier nmclassifier;
     KNMClassifier knmClassifier;
     Fisher fisher;
     Sfs sfs;
     std::vector<Object> trainPartOfObjects;
     std::vector<Object> testPartOfObjects;
     std::vector<std::vector<Object>> partsOfObjects;
};

#endif // MAINWINDOW_H
