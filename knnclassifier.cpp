#include "knnclasifier.h"

int KNNClassifier::execute(std::vector<Object> trainingSet, std::vector<Object> testingSet, int k) {

    for (Object testingObject : testingSet)

        // sprawdzenie czy zwrócona przypisana wartość klasy do testowego obiektu jest taka sama jaką ma ten obiekt
        if (classify(trainingSet, testingObject, k).compare(testingObject.getClassName()) == 0)
            properlyClassified++;
        else
            misclassified++;

    // zwrócenie % wartości prawidłowych trafień
    return ((double)properlyClassified/((double)properlyClassified + (double)misclassified))*100;
}


std::string KNNClassifier::classify(std::vector<Object> trainingSet, Object testingObject, int k) {

    // stworzenie mapy wszystkich odległości między obiektami - klucz dystans, wartość to klasa
    std::map<double, std::string> distanceResults;

    for (Object trainingObject: trainingSet)
        distanceResults[countEuclideanDistance(testingObject, trainingObject)] = trainingObject.getClassName();

    // zwrócenie najbliższej próbki
    return takeNearestSample(distanceResults, k);
}

double KNNClassifier::countEuclideanDistance(Object object, Object target) {
    double sum = 0.0;
    for (int i = 0; i < object.getFeaturesNumber(); i++)
        sum += countDistance(object, target, i);
    return sum;
}

//przepisanie do mapy najblizszych k-obiektów (mapa jest sortowana wedlug klucza)
std::string KNNClassifier::takeNearestSample(std::map<double, std::string> distanceResults, int k) {

    /* SPRAWDZIC POOWANIE */

    std::map<double, std::string> newMap;
    std::map<double, std::string>::iterator t = distanceResults.begin();

    for (int i = 0; i < k; i++) {
        newMap[t->first] = t->second;
        t++;
    }

    return (getTheMostCommonElementInMap(newMap));
}

// wyzerowanie wartosci ile obiektow nalezy do klasy A i ile do klasy B , sprawdzenie jaka klasa i przypisanie
std::string KNNClassifier::getTheMostCommonElementInMap(std::map<double, std::string> winSample) {

    int classA = 0;
    int classB = 0;

    for(std::map<double, std::string>::iterator it = winSample.begin(); it != winSample.end(); ++it) {
        if (winSample[it->first].compare("Acer") == 0)
            classA++;
        else
            classB++;
    }

    // sprawdzenie ktorych obiektow danej klasy jest wiecej
    return classA>classB?"Acer":"Quercus";  //Hardcoded

}

double KNNClassifier::countDistance(Object object, Object target, int noFeatures) {
    return pow(object.getFeatures().at(noFeatures) - target.getFeatures().at(noFeatures), 2);
}
