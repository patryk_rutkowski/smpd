#include "knmclassifier.h"
#include <math.h>
#include "mean.h"

int KNMClassifier::execute(std::vector<Object> trainingSet, std::vector<Object> testingSet, int k){

    clear();
    divideValues(trainingSet);

    // przypisujemy pierwszym k wartościom różne podklasy (k podklas) i tworzymy dla każdej podklasy obiekt średniej cech (mean) (obecnie równa temu 1 elementowi)
    for(int i=0; i<k; i++){
        quercusValues[i].setSubclassID(i);
        quercusSubclassMeans.push_back(Mean(quercusValues.at(i).object.getFeatures()));
        acerValues[i].setSubclassID(i);
        acerSubclassMeans.push_back(Mean(acerValues.at(i).object.getFeatures()));
    }

    // rozdzielamy elementy pomiedzy podklasy sprawdzajac najblizsze odleglosci obiektow do srednich
    quercusSubclassMeans = calculateSubclassTrain(quercusValues, quercusSubclassMeans);
    acerSubclassMeans = calculateSubclassTrain(acerValues, acerSubclassMeans);

    // wyliczmy % poprawnej klasyfikacji obiektów testowych
    return calculatePercentageRightClassificationOfTestObjects(testingSet);
}


std::vector<Mean> KNMClassifier::calculateSubclassTrain(std::vector<SubclassObject> values, std::vector<Mean> means){
    // licznik zmian przejść między podklasami
    int changeCounter;

    // robimy dopóki licznik jest różny od 0
    do{
        changeCounter = 0;
        // tworzymy wektor dla "nowych średnich" i czyścimy średnie
        std::vector<Mean> newMeans = means;

        for(unsigned int i=0; i<newMeans.size();i++)
        {
            newMeans[i].clear();
        }


        for (unsigned int i=0; i<values.size();i++){
            int subclassId = calculateNearestSubclassAndReturnSubclassId(values[i].object, means);
            // sprawdzamy czy podklasa do ktorej obiekt nalezy jest taka sama jak wyliczona obecnie jesli nie dokonujemy zmiany i zwiekszamy licznik
            if(values[i].getSubclassID() != subclassId){
                values[i].setSubclassID(subclassId);
                changeCounter++;
            }
            // dodajemy do sredniej danej podklasy nowy element
            newMeans[subclassId].addFeaturesToSum(values[i].object.getFeatures());
        }

        //dokonujemy przeliczenia srednich na podstawie dodanych wartosci
        for(unsigned int i=0; i<newMeans.size();i++)
        {
            newMeans[i].calculateFeaturesMean();
        }

        // przypisujemy nowe srednie do głównych srednich
        means = newMeans;

    }while(changeCounter != 0);

    // zwracamy główne srednie
    return means;
}

// przeliczamy najbliżej której średniej jest sprawdzany obiekt aby sklasyfikowac do ktorej podklasy nalezy
int KNMClassifier::calculateNearestSubclassAndReturnSubclassId(Object o, std::vector<Mean> means){
    int id = 0;
    double lowestValue = -1.0;

    // znajdujemy najblizsza wartosc euklidesowa i zwracamy id tej podklasy
    for(unsigned int i=0; i<means.size(); i++){
        double computeRet = euclid.computeComparation(means[i].getFeaturesMean(), o.getFeatures(), o.getFeaturesNumber());

        if((computeRet < lowestValue) || (lowestValue == -1.0))
        {
             lowestValue = computeRet;
             id = i;
        }
    }
    return id;
}

// dzielenie wektora treningowego na dwa osobne dla każdej klasy
void KNMClassifier::divideValues(std::vector<Object> trainingSet) {

    for (Object trainingObject : trainingSet) {

        if(trainingObject.getClassName().compare("Quercus") == 0)
            quercusValues.push_back(SubclassObject(trainingObject));
        else
            acerValues.push_back(SubclassObject(trainingObject));
    }
}

// wyliczenie prawidłowych klasyfikacji obiektów z wektora testowego
int KNMClassifier::calculatePercentageRightClassificationOfTestObjects(std::vector<Object> testingSet){
    int calcRightClassification = 0;
    for(Object oTest : testingSet){
        if(compareTestObjectWithTrainMeans(oTest))
            calcRightClassification++;
    }

    return (calcRightClassification*100)/testingSet.size();
}

// porównanie obiektu testowego ze srednimi podklas
bool KNMClassifier::compareTestObjectWithTrainMeans(Object testObject){
    std::vector<float> testFeatures = testObject.getFeatures();
    int numberOfFeatures = testObject.getFeaturesNumber();

    double lowestValue = -1.0;
    std::string trainClassName = "";

    // wyliczamy najblisza odleglosc do srednich podklas Acera
    for(Mean meanTrain : acerSubclassMeans){

        double computeRet = euclid.computeComparation(meanTrain.getFeaturesMean(), testFeatures, numberOfFeatures);

        if((computeRet < lowestValue) || (lowestValue == -1.0))
        {
            lowestValue = computeRet;
            trainClassName = "Acer";
        }
    }

    // wyliczamy najblisza odleglosc do srednich podklas Quercusa (jak blizej nadpiszemy Acera)
    for(Mean meanTrain : quercusSubclassMeans){

        double computeRet = euclid.computeComparation(meanTrain.getFeaturesMean(), testFeatures, numberOfFeatures);

        if((computeRet < lowestValue) || (lowestValue == -1.0))
        {
            lowestValue = computeRet;
            trainClassName = "Quercus";
        }
    }

    // sprawdzamy czy obiekt zostal sklasyfikowany tak jak wedlug sredniej
    return trainClassName.compare(testObject.getClassName()) == 0;
}

// czyszczenie wszystkich wektorów
void KNMClassifier::clear(){
    quercusValues.clear();
    acerValues.clear();
    quercusSubclassMeans.clear();
    acerSubclassMeans.clear();
}

