#ifndef NMCLASSIFIER_H
#define NMCLASSIFIER_H



#include <vector>
#include <map>
#include "object.h"

class NMClassifier
{
    private:
        int properlyClassified = 0, missClassified = 0;
        std::vector<Object> quercusValues, acerValues;
        std::vector<double> meanQuercusValues, meanAcerValues;
        void classify(Object testingObject) ;
        void divideValues(std::vector<Object> trainingSet);
        void calculateMeanValue(std::vector<Object> objectsVector,std::vector<double> &destinyVector);
        double calculateDistance(std::vector<double> meanVector, Object testingObject);


    public:
        NMClassifier(){}
        int execute(std::vector<Object> trainingSet, std::vector<Object> testingSet);
};

#endif // NMCLASSIFIER_H
