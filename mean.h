#ifndef MEAN_H
#define MEAN_H

#include <vector>
#include <map>
#include "object.h"

class Mean
{
    private:
    std::vector<float> featuresSum;
    std::vector<float> featuresMean;
    int amountOfAddedObjects=0;

    public:

        Mean(std::vector<float> initialValue){
            featuresSum = initialValue;
            amountOfAddedObjects=1;
            featuresMean = featuresSum;
        }

        void clear(){featuresSum.clear(); amountOfAddedObjects = 0; featuresMean.clear();}

        std::vector<float> getFeaturesMean(){

            return featuresMean;
        }

        int getAmountOfAddedObjects(){
            return amountOfAddedObjects;
        }

        void calculateFeaturesMean(){

            if(amountOfAddedObjects==1){
                featuresMean = featuresSum;
            }
            std::vector<float> meanVector;
            for (unsigned int i = 0; i < featuresSum.size(); i++) {
                    meanVector.push_back(featuresSum.at(i)/amountOfAddedObjects);
            }
            featuresMean = meanVector;
        }

        void addFeaturesToSum(std::vector<float> features){

            if(featuresSum.size() == 0){
                featuresSum = features;
                amountOfAddedObjects = 1;
                return;
            }

            for (unsigned int i = 0; i < featuresSum.size(); i++) {
                featuresSum[i] += features[i];
            }
            amountOfAddedObjects++;
        }
};

#endif // MEAN_H
