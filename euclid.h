#ifndef EUCLID_H
#define EUCLID_H

#include <vector>
#include <map>
#include "object.h"

class Euclid
{
    public:
    double computeComparation(std::vector<float> trainFeatures, std::vector<float> testFeatures, int numberOfFeatures);
};

#endif // EUCLID_H
