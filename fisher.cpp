#include "fisher.h"

std::vector<int> Fisher::execute(std::vector<Object> trainingObjects, int requiredFeaturesQuantity) {


    this->requiredFeaturesQuantity = requiredFeaturesQuantity;
    this->trainingSet = trainingObjects;

    std::map<std::string, std::vector<Object>> classesLists = MatrixUtil::separate(trainingObjects);
    std::map<std::string, boost::numeric::ublas::matrix<float>> classesMatrices = MatrixUtil::prepareClassesMatrices(classesLists);

    std::vector<boost::numeric::ublas::matrix<float>> covarianceMatrices;
    std::vector<std::vector<float>> means;

    int count = 0;

    for (std::map<std::string, boost::numeric::ublas::matrix<float>>::iterator it = classesMatrices.begin(); it != classesMatrices.end(); ++it) {
        means.push_back(MatrixUtil::calculateMeans(classesMatrices.at(it->first)));
        covarianceMatrices.push_back(MatrixUtil::calculateCovarianceMatrix(MatrixUtil::subtractMeansFromClass(classesMatrices.at(it->first), means[count++])));
    }

    std::vector<std::vector<int>> combinations = calculateOptions(63, requiredFeaturesQuantity);
    std::map<std::vector<int>, float> fishers = calculateFisher(covarianceMatrices, means, combinations);

    std::vector<int> bestFeatures;
    int max = 0;
    for( std::map<std::vector<int>, float>::iterator it = fishers.begin(); it != fishers.end(); it++ ) {
        if(max < it->second) {
            max = it->second;
            bestFeatures = it->first;
        }
    }

    return bestFeatures;
}

/**
  Wybranie najlepszych cech
 * @brief Fisher::chooseBestFeatures
 * @param fishers
 * @param requiredFeaturesQuantity
 * @return
 */
std::vector<int> chooseBestFeatures(std::map<std::vector<int>, float> fishers)
{
    std::vector<int> bestFeatures;
    int max = 0;

    for( std::map<std::vector<int>, float>::iterator it = fishers.begin(); it != fishers.end(); it++ ) {
        if(max < it->second) {
            max = it->second;
            bestFeatures = it->first;
        }
    }

    return bestFeatures;
}


/**
  Obliczenie wszystkich "Fisherów" ze wzoru: (||Ua - Ub||)/(det(Sa) + det(Sb))
 * @brief Fisher::calculateFisher
 * @param covarianceMatrices
 * @param means
 * @param combinations
 * @return
 */
std::map<std::vector<int>, float> Fisher::calculateFisher(std::vector<boost::numeric::ublas::matrix<float>> covarianceMatrices, std::vector<std::vector<float>> means, std::vector<std::vector<int>> combinations)
{
    std::map<std::vector<int>, float> fisherParameters;

    boost::numeric::ublas::matrix<float> meanA(combinations.at(0).size(), 1), meanB(combinations.at(0).size(), 1);
    boost::numeric::ublas::matrix<float> sa(combinations.at(0).size(), combinations.at(0).size()), sb(combinations.at(0).size(), combinations.at(0).size());

    for (int allCombinations = 0; allCombinations < combinations.size(); allCombinations++) {

        meanA.clear();meanB.clear();sa.clear();sb.clear();

        for (int combinationsAtSpecyficPoint = 0; combinationsAtSpecyficPoint < combinations.at(allCombinations).size(); combinationsAtSpecyficPoint++) {
            //Uzupełnienie tablicy z macierzy ze średnimi
            meanA(combinationsAtSpecyficPoint, 0) = means.at(0).at(combinations.at(allCombinations).at(combinationsAtSpecyficPoint));
            meanB(combinationsAtSpecyficPoint, 0) = means.at(1).at(combinations.at(allCombinations).at(combinationsAtSpecyficPoint));

            //Uzupełnienie tablicy z macierzy kowariancji
            for (int combinationForMatrix = 0; combinationForMatrix < combinations.at(allCombinations).size(); combinationForMatrix++) {
                sa(combinationsAtSpecyficPoint, combinationForMatrix) = covarianceMatrices.at(0)(combinations.at(allCombinations).at(combinationsAtSpecyficPoint), combinations.at(allCombinations).at(combinationForMatrix));
                sb(combinationsAtSpecyficPoint, combinationForMatrix) = covarianceMatrices.at(1)(combinations.at(allCombinations).at(combinationsAtSpecyficPoint), combinations.at(allCombinations).at(combinationForMatrix));
            }

        }

        //Obliczenia
        float distance = MatrixUtil::matrixDistance(meanA - meanB);
        float detSum = MatrixUtil::det_fast(sa) + MatrixUtil::det_fast(sb);

        fisherParameters[combinations.at(allCombinations)] = distance/detSum;
    }

    return fisherParameters;
}

/**
  Obliczenie kombinacji bez powtórzeń dla podanej liczby elemntów
 * @brief Fisher::calculateOptions
 * @param max
 * @param size
 * @return
 */
std::vector<std::vector<int>> Fisher::calculateOptions(const int max, const int size) {
    int *array = new int[size];

    std::vector<std::vector<int>> combinations;
    std::vector<int> test;

    for (int i = 0; i < size; i++)
    {
        array[i] = i;
    }

    bool done = false;
    while (!done)
    {
        for(int i = 0; i < size; i++) {
            test.push_back(array[i]);
        }

        bool add = true;

        for (int i = 0; i < test.size(); i++) {
            int actualNumber = test.at(i);

            for (int z = i + 1; z < test.size(); z++)
                if (actualNumber == test.at(z))
                    add = false;
        }

        if (add)
            combinations.push_back(test);

        test.clear();


        int index = size - 1;
        int position_max = max;
        while(index >= 0 && array[index] == position_max)
        {
          position_max--;
          index--;
        }
        if (index < 0)
        {
          done = true;
        }
        else
        {
            array[index]++;
            for(int i = index + 1; i < size; i++)
            {
              array[i] = array[i-1];
            }
        }
    }

    return combinations;
}
