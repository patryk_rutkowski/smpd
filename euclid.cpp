#include "euclid.h"
#include <math.h>

double Euclid::computeComparation(std::vector<float> trainFeatures, std::vector<float> testFeatures, int numberOfFeatures)
{
    double sum = 0.0;
    for(int i = 0; i < numberOfFeatures; i++){
        sum += pow(testFeatures[i] - trainFeatures[i], 2.0);
    }

    return sqrt(sum);
}
