#include "nmclassifier.h"
#include <QDebug>
#include <QString>
#include <QTextCodec>

//
void NMClassifier::classify(Object testingObject) {

    // porównujemy odległość obiektu testowego do średniej Acera i Quercusa i tam gdzie ma bliżej tak go klasyfikujemy
    std::string classifiedName = calculateDistance(meanQuercusValues, testingObject)>calculateDistance(meanAcerValues, testingObject)?"Acer":"Quercus";

    // sprawdzenie czy klasyfikacja dokonana została poprawnie
    if (strcmp( testingObject.getClassName().c_str(), classifiedName.c_str()) == 0)
        properlyClassified++;
    else
        missClassified++;
}

int NMClassifier::execute(std::vector<Object> trainingSet, std::vector<Object> testingSet) {
    meanQuercusValues.clear();
    meanAcerValues.clear();
    quercusValues.clear();
    acerValues.clear();

    divideValues(trainingSet);
    calculateMeanValue(quercusValues, meanQuercusValues);
    calculateMeanValue(acerValues, meanAcerValues);

    // klasyfikujemy
    for (Object testObject : testingSet) {
        classify(testObject);
    }

    return ((double)properlyClassified/((double)properlyClassified + (double)missClassified))*100;
}

// podzielenie wektora treningowego na dwa osobne dla każdej klasy
void NMClassifier::divideValues(std::vector<Object> trainingSet) {

    for (Object trainingObject : trainingSet) {

        if(trainingObject.getClassName().compare("Quercus") == 0)
            quercusValues.push_back(trainingObject);
        else
            acerValues.push_back(trainingObject);
    }

}

// wyliczenie średnich wartości
void NMClassifier::calculateMeanValue(std::vector<Object> objectsVector,std::vector<double> &destinyVector) {

    double sum = 0;
    // pobieramy liczbę cech obiektu
    int no = objectsVector.at(0).getFeatures().size();

    // przechodzimy przez wszystkie cechy i sumujemy wszystkie cechy o tych samych indeksach
    for (int i = 0; i < no; i++) {
        for (int j = 0; j < objectsVector.size(); j++) {
            sum += objectsVector.at(j).getFeatures()[i];
        }

        // wrzucamy na wektor średnich - srednie dla kazdej cechy z wszystkich elementów danej klasy
        destinyVector.push_back(sum/objectsVector.size());
        sum = 0;
    }

}

// oblieczenie odleglości od obiektu testowego do srednich wartosci
double NMClassifier::calculateDistance(std::vector<double> meanVector, Object testingObject) {

    double value = 0;

    for (int i = 0; i < meanVector.size(); i++) {
        value += pow(testingObject.getFeatures()[i] - meanVector.at(i), 2.0);
    }

    return sqrt(value);
}
