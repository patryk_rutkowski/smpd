#ifndef KNMCLASIFIER_H
#define KNMCLASIFIER_H

#include <vector>
#include <map>
#include "subclassObject.h"
#include "euclid.h"
#include "mean.h"

class KNMClassifier
{
    private:
            Euclid euclid;
            std::vector<SubclassObject> quercusValues;
            std::vector<SubclassObject> acerValues;
            std::vector<Mean> quercusSubclassMeans;
            std::vector<Mean> acerSubclassMeans;

            void divideValues(std::vector<Object> trainingSet);
            std::vector<Mean> calculateSubclassTrain(std::vector<SubclassObject> values, std::vector<Mean> means);
            int calculateNearestSubclassAndReturnSubclassId(Object o, std::vector<Mean> means);
            void clear();
            int calculatePercentageRightClassificationOfTestObjects(std::vector<Object> testingSet);
            bool compareTestObjectWithTrainMeans(Object testObject);
    public:
            KNMClassifier(){}
            int execute(std::vector<Object> trainingSet, std::vector<Object> testingSet, int k);
};

#endif // KNMCLASIFIER_H
