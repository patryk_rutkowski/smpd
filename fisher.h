#ifndef FISHER_H
#define FISHER_H

#include <map>
#include <vector>
#include <string>
#include "matrixutil.hpp"
#include "matrixutil.h"
#include "object.h"

class Fisher
{

    private:
        int requiredFeaturesQuantity;
        std::map<int, float> bestFeatures;
        std::vector<Object> trainingSet;

        template<typename ValType>
        ValType det_fast(const boost::numeric::ublas::matrix<ValType>& matrix);
        std::vector<int> chooseBestFeatures(std::map<std::vector<int>, float> fishers);
        std::map<std::vector<int>, float> calculateFisher(std::vector<boost::numeric::ublas::matrix<float>> covarianceMatrices, std::vector<std::vector<float>> means, std::vector<std::vector<int>> combinations);
        std::vector<std::vector<int>> calculateOptions(const int max, const int size);
        float determinant( boost::numeric::ublas::matrix<float>& m );
        int determinant_sign(const boost::numeric::ublas::permutation_matrix<std ::size_t>& pm);


    public:
        Fisher(){}
        std::vector<int> execute(std::vector<Object> trainingObjects, int requiredFeaturesQuantity);

};

#endif // FISHER_H
