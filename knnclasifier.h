#ifndef KNNCLASIFIER_H
#define KNNCLASIFIER_H

#include <vector>
#include <map>
#include <math.h>
#include "object.h"

class KNNClassifier
{
    private:
        int properlyClassified = 0, misclassified = 0;
        std::string classify(std::vector<Object> trainingSet, Object testingObject, int k);
        double countEuclideanDistance(Object object, Object target);
        std::string getTheMostCommonElementInMap(std::map<double, std::string> winSample);
        std::string takeNearestSample(std::map<double, std::string> distanceResults, int k);
        double countDistance(Object object, Object target, int noFeatures);

    public:
            KNNClassifier(){}
            int execute(std::vector<Object> trainingSet, std::vector<Object> testingSet, int k);
};

#endif // KNNCLASIFIER_H
