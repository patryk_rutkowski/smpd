#include "classifierCalc.h"

    // główna metoda dokonująca operacji w metodzie NN
    int ClassifierCalc::execute(std::vector<Object> testObjects, std::vector<Object> trainObjects)
    {

        this->testObjects = testObjects; noOfTestObjects = testObjects.size();
        this->trainObjects = trainObjects; noOfTrainObjects = trainObjects.size();

         // zerowana wartość prawidłowych klasyfikacji
        int calcRightClassification = 0;

        // przejście po obiektach testowych i sprawdzenie czy są dobrze sklasyfikowane (sprawdzenie czy najblizszy treningowy jest tej samej klasy)
        for(Object oTest : testObjects){
            if(compareTestObjectWithTrainVector(oTest))
                calcRightClassification++;
        }
        // % wyliczenie prawidłowych klasyfikacji
        return (calcRightClassification*100)/noOfTestObjects;
    }

    bool ClassifierCalc::compareTestObjectWithTrainVector(Object testObject)
    {
        std::vector<float> testFeatures = testObject.getFeatures();
        size_t numberOfFeatures = testObject.getFeaturesNumber();

        double lowestValue = -1.0;
        std::string trainClassName = "";

        // przejście po wszystkich obiektach treningowych i określenie, który jest najbliżej testowego
        for(Object oTrain : trainObjects){

            double computeRet = computeComparation(oTrain.getFeatures(), testFeatures, numberOfFeatures);

            // jesli wartość jest mniejsza od wcześniej zapisanej najniższej to ją podmieniamy
            if((computeRet < lowestValue) || (lowestValue == -1.0))
            {
                lowestValue = computeRet;
                trainClassName = oTrain.getClassName();
            }

        }
        // wywołanie sprawdzenia czy obiekt został dobrze sklasyfikowany
        return trainClassName.compare(testObject.getClassName()) == 0;
    }

    // obliczenie odległości wszystkich cech testowych i treningowych i zwrócenie jej wartości
    double ClassifierCalc::computeComparation(std::vector<float> trainFeatures, std::vector<float> testFeatures, int numberOfFeatures)
    {
        double sum = 0.0;
        for(int i = 0; i < numberOfFeatures; i++){
            sum += pow(testFeatures[i] - trainFeatures[i], 2.0);
        }

        return sqrt(sum);
    }
