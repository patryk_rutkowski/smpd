#ifndef SFS_H
#define SFS_H

#include <map>
#include <vector>
#include <string>
#include <math.h>
#include "matrixutil.hpp"
#include "object.h"

using namespace std;
using namespace boost;
using namespace numeric;
using namespace ublas;

class Sfs
{
    private:
        std::vector<int> chooseBestFeatures(int fisherVal, int requiredFeaturesQuantity, std::map<string, matrix<float>> covarianceMatrices, std::map<string, std::vector<float>> means);
        int chooseNextBestFeature(std::vector<int> bestFeatures, std::map<string, matrix<float>> covarianceMatrices, std::map<string, std::vector<float>> means);
        matrix<float> getMatrixForBestFeaturesAndAnalizedOne(std::vector<int> bestFeaturesIndexes, std::vector<float> meansForClass, float analizedFeatureValue);
        bool isNotAlreadyBestFeature(std::vector<int> bestFeatures, int meanIndex);
        matrix<float> getPartialCovarianceMatrix(matrix<float> covarianceMatrices, std::vector<int> featuresIndexes);

    public:
        Sfs(){}
        std::vector<int> execute(std::vector<Object> trainingObjects, int requiredFeaturesQuantity, int fisherVal1);
};

#endif // SFS_H
