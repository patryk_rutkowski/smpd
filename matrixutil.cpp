#include "matrixutil.hpp"

// podzielenie wartości treningowych ze względu na klasę - mapa <string, wektor> z kluczem nazwy klasy, a wartościami są obiekty
std::map<std::string, std::vector<Object>> MatrixUtil::separate(std::vector<Object> trainingObjects) {
    std::map<std::string, std::vector<Object>> classes;

    std::vector<Object> quercusValues;
    std::vector<Object> acerValues;

    for(Object value : trainingObjects) {

        if (value.getClassName().compare("Quercus") == 0)
            quercusValues.push_back(value);
        else
            acerValues.push_back(value);
    }

    classes["Quercus"] = quercusValues;
    classes["Acer"] = acerValues;

    return classes;

}

// przetworzenie na mape <string, macierz danej klasy>
std::map<std::string, boost::numeric::ublas::matrix<float>> MatrixUtil::prepareClassesMatrices(std::map<std::string, std::vector<Object>> classes)
{
    std::map<std::string, boost::numeric::ublas::matrix<float>> classesMatrices;
    classesMatrices["Quercus"] = prepareClassMatrix(classes["Quercus"]);
    classesMatrices["Acer"] = prepareClassMatrix(classes["Acer"]);
    return classesMatrices;

}

// odjęcie wartości średnich od klasy
boost::numeric::ublas::matrix<float> MatrixUtil::subtractMeansFromClass(boost::numeric::ublas::matrix<float> singleClass, std::vector<float> means)
{
    size_t rows = singleClass.size1();
    size_t cols = singleClass.size2();
    boost::numeric::ublas::matrix<float> intraclassScattering(rows, cols);


    for (int row = 0; row < rows; row++)
        for (int col = 0; col < cols; col++)
            intraclassScattering(row, col) = singleClass(row, col) - means.at(col);

    return intraclassScattering;
}

// przygotowanie macierzy : wiersze - ilość klas, kolumny - ilość cech
boost::numeric::ublas::matrix<float> MatrixUtil::prepareClassMatrix(std::vector<Object> singleClass) {
    size_t noClass = singleClass.size();
    size_t noFeatures = singleClass[0].getFeaturesNumber();
    boost::numeric::ublas::matrix<float> classFeatures(noClass, noFeatures);

    for (int row = 0; row < noClass; row++) {
        for (int col = 0; col < noFeatures; col++) {
            classFeatures(row, col) = singleClass[row].getFeatures()[col];
        }
    }

    return classFeatures;
}

// z macierzy danej klasy wylicza wektor średnich wartości dla cech
std::vector<float> MatrixUtil::calculateMeans(boost::numeric::ublas::matrix<float> singleClass) {

    std::vector<float> meanMatrix;

    for (int i = 0; i < singleClass.size2(); i++) {
        float sum = (float) 0;

        for (int j = 0; j < singleClass.size1(); j++) {
            sum += singleClass(j, i);
        }

        meanMatrix.push_back(sum / (float) singleClass.size1());
    }

    return meanMatrix;
}

// wyliczenie macierzy kowariancji czyli macierz * macierz transponowana
boost::numeric::ublas::matrix<float> MatrixUtil::calculateCovarianceMatrix(boost::numeric::ublas::matrix<float> singleClass) {
    boost::numeric::ublas::matrix<float> transposedClass;
    transposedClass = boost::numeric::ublas::trans(singleClass);
    boost::numeric::ublas::matrix<float> prodp = boost::numeric::ublas::prod(transposedClass, singleClass);

    for (int i = 0 ; i < prodp.size1(); i++ )
        for (int j = 0; j < prodp.size2(); j++)
            prodp(i, j) = prodp(i,j)/singleClass.size1();


    return prodp;
}

// obliczenie odległości różnicy porównywanych cech
float MatrixUtil::matrixDistance(boost::numeric::ublas::matrix<float> m)
{
    double sum = 0.0;
    for(int i=0; i<m.size1();i++){
        sum += pow(m(i,0),2.0);
    }

    return sqrt(sum);
}

// metoda do wyliczania wyznacznika macierzy
float MatrixUtil::det_fast(const boost::numeric::ublas::matrix<float>& matrix)
{
    // create a working copy of the input
    boost::numeric::ublas::matrix<float> mLu(matrix);
    boost::numeric::ublas::permutation_matrix<std::size_t> pivots(matrix.size1());

    auto isSingular = boost::numeric::ublas::lu_factorize(mLu, pivots);
    if (isSingular)
        return static_cast<float>(0);

    float det = static_cast<float>(1);
    for (std::size_t i = 0; i < pivots.size(); ++i)
    {
        if (pivots(i) != i)
            det *= static_cast<float>(-1);

        det *= mLu(i, i);
    }

    return det;
}
