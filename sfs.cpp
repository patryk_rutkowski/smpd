#include "sfs.h"

std::vector<int> Sfs::execute(std::vector<Object> trainingObjects, int requiredFeaturesQuantity, int fisherVal1){
    std::vector<int> fisherVal;
    fisherVal.push_back(fisherVal1);
    //jeśli wybrana wartość = 1 zwracamy fishera od 1
    if(requiredFeaturesQuantity == 1){
        return fisherVal;
    }


    std::map<string, std::vector<Object>> classesLists = MatrixUtil::separate(trainingObjects);
    std::map<string, matrix<float>> classesMatrices = MatrixUtil::prepareClassesMatrices(classesLists);

    // utworzenie map macierzy kowariancji i srednich
    std::map<string, matrix<float>> covarianceMatrices;
    std::map<string, std::vector<float>> means;

    // obliczenie macierzy kowariancji z macierzy pomniejszonej o wartosc srednich i przypisanie srednich do mapy
    means["Quercus"] = MatrixUtil::calculateMeans(classesMatrices.at("Quercus"));
    covarianceMatrices["Quercus"] = MatrixUtil::calculateCovarianceMatrix(MatrixUtil::subtractMeansFromClass(classesMatrices.at("Quercus"), means["Quercus"]));

    means["Acer"] = MatrixUtil::calculateMeans(classesMatrices.at("Acer"));
    covarianceMatrices["Acer"] = MatrixUtil::calculateCovarianceMatrix(MatrixUtil::subtractMeansFromClass(classesMatrices.at("Acer"), means["Acer"]));

    return chooseBestFeatures(fisherVal.at(0), requiredFeaturesQuantity, covarianceMatrices, means);
}

// wybór najlepszych cech
std::vector<int> Sfs::chooseBestFeatures(int fisherVal, int requiredFeaturesQuantity, std::map<string, matrix<float>> covarianceMatrices, std::map<string, std::vector<float>> means){

    // ustalamy wektor najlepszych cech i wrzucamy do niego jako pierwsza ceche wybrana z fishera dla 1
    std::vector<int> bestFeatures;
    bestFeatures.push_back(fisherVal);

    // w pętli wybieramy i wrzucamy najlepsze następne cechy
    for(int i=1; i<requiredFeaturesQuantity; i++){
        bestFeatures.push_back(chooseNextBestFeature(bestFeatures, covarianceMatrices, means));
    }

    return bestFeatures;
}

int Sfs::chooseNextBestFeature(std::vector<int> bestFeatures, std::map<string, matrix<float>> covarianceMatrices, std::map<string, std::vector<float>> means){
    int index = 0;
    float bestValue = -1;

    size_t noOfFeatures = means.at("Quercus").size();

    for(int i=0;i<noOfFeatures;i++){

        // sprawdzamy czy cecha jest juz na liscie najlepszych cech jesli nie to wykonujemy operacje
        if(isNotAlreadyBestFeature(bestFeatures, i)){

            //pobranie macierzy z najlepszymi cechami i cechy, która aktualnie analizujemy
            matrix<float> MA = getMatrixForBestFeaturesAndAnalizedOne(bestFeatures, means.at("Quercus"), means.at("Quercus").at(i));
            matrix<float> MB = getMatrixForBestFeaturesAndAnalizedOne(bestFeatures, means.at("Acer"), means.at("Acer").at(i));

            // wyliczamy odleglosc z roznicy srednich porównywanych cech
            matrix<float> MAB = MA - MB;
            float matrixDist = MatrixUtil::matrixDistance(MAB);

            // najlepsze cechy wpisujemy do zmiennej pomocniczej (tylko ich indeksy) + ten aktualnie analizowany
            std::vector<int> featuresTmp = bestFeatures;
            featuresTmp.push_back(i);
            // z macierzy kowariancji wybieramy tylko elementy o porównywanych indeksach i z tego wyliczamy wyznacznik dla A i B
            matrix<float> covarianceForA = getPartialCovarianceMatrix(covarianceMatrices.at("Quercus"), featuresTmp);
            float detA = MatrixUtil::det_fast(covarianceForA);

            matrix<float> covarianceForB = getPartialCovarianceMatrix(covarianceMatrices.at("Acer"), featuresTmp);
            float detB = MatrixUtil::det_fast(covarianceForB);

            // wyliczenie wzoru odległość / sumę wyznaczników i sprawdzamy czy wartość jest większa od obecnej najlepszej jeśli tak to ją przypisujemy jako najlepszą
            if((matrixDist/(detA+detB))>bestValue){
                bestValue = matrixDist/(detA+detB);
                index = i;
            }
        }
    }

    return index;
}

// wybranie z macierzy tylko tych elementów do nowej macierzy, które maja współrzędne równe indeksom najlepszych cech + obecnie porównywanej
matrix<float> Sfs::getPartialCovarianceMatrix(matrix<float> covarianceMatrices, std::vector<int> featuresIndexes){

    matrix<float> partialMatrix(featuresIndexes.size(), featuresIndexes.size());
    std::sort (featuresIndexes.begin(), featuresIndexes.end());

    for(int row=0;row<featuresIndexes.size();row++){
        for(int col=0;col<featuresIndexes.size();col++){
            partialMatrix(row,col) = covarianceMatrices(featuresIndexes.at(row),featuresIndexes.at(col));
        }
    }

    return partialMatrix;
}
// sprawdzamy czy na liscie najlepszych cech jest juz ta cecha
bool Sfs::isNotAlreadyBestFeature(std::vector<int> bestFeatures, int meanIndex){

    if(std::find(bestFeatures.begin(), bestFeatures.end(), meanIndex) != bestFeatures.end()) {
        return false;
    }
    return true;
}

// pobiera macierz, która zawiera srednie najlepszych cech i tej, którą aktualnie analizujemy
matrix<float> Sfs::getMatrixForBestFeaturesAndAnalizedOne(std::vector<int> bestFeaturesIndexes, std::vector<float> meansForClass, float analizedFeatureValue){

    matrix<float> featuresMatrixM(bestFeaturesIndexes.size()+1, 1);

    for(int i=0; i<bestFeaturesIndexes.size(); i++){
        featuresMatrixM(i, 0) = meansForClass.at(bestFeaturesIndexes.at(i));
    }

    featuresMatrixM(bestFeaturesIndexes.size(),0) = analizedFeatureValue;

    return featuresMatrixM;
}
