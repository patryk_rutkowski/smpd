#ifndef SUBCLASSOBJECT_H
#define SUBCLASSOBJECT_H

#include <string>
#include <vector>
#include "object.h"

class SubclassObject
{
    private:
        int subclassID=0;

    public:
        Object object;
        SubclassObject(Object o){ object = o;}

        int getSubclassID(){return subclassID;}
        void setSubclassID(int id){subclassID = id;}
};



#endif // SUBCLASSOBJECT_H
